# Lar São Domingos

Este é um projeto, que será realizado durante as disciplinas do 5º período.


## Configuração

executar no cmd:
- cd [pasta do projeto]
- npm install
- No arquivo .env configurar a porta(PORT) desejada

> **Observação**: Node >= v6

## Executar

-  rodar o comando no cmd: npm start
      > Server running 7000
- Navegador: http://localhost:7000/
	 > { msg:  "Bem-vindo !" }
 
 > **Obs**: 7000 deve ser o valor da porta indicada no arquivo .env