var express = require('express');
var dotenv = require('dotenv');
var consign = require('consign');
var app = express();

// Carrega as variaveis de ambiente em modo de desenvolvimento 
// Variaveis no arquivo .env
if (process.env.NODE_ENV != 'production') dotenv.load();

consign({verbose:false})
    .include('config')
    .then('controllers')
    .then('routes')
    .then('app.js')
    .into(app);