// function de Controller
// Regra de Negócio deve ser validadas aqui

module.exports = app => {
    let ctrl = {};

    ctrl.home = (req, res) => {
        res.status(200).json({
            msg: 'Bem-vindo !'
        });
    }

    return ctrl;
}